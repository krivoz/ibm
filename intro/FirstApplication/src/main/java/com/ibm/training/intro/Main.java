package com.ibm.training.intro;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = 
				        new ClassPathXmlApplicationContext("beans.xml");
		GreetingService greetingService = 
                        (GreetingService) context.getBean("greetingServiceImpl");
		System.out.println(greetingService.greeting());
		System.out.println(greetingService.getCount());
		
		GreetingService greetingService2 = 
                (GreetingService) context.getBean("greetingServiceImpl");
		System.out.println(greetingService2.greeting());

		System.out.println(greetingService2.getCount());
		
		
		MyRepository myRepository = 
                (MyRepository) context.getBean("myRepository");
		myRepository.save();
		
		
		MyService myService = 
                (MyService) context.getBean("myService");
		myService.save();

		
		((ConfigurableApplicationContext) context).close();

	}

}
