package com.ibm.training.intro;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//@Component
//@Scope("prototype")
public class GreetingServiceImpl 
implements GreetingService {
	
	private int count = 0;

	public String greeting() {
		// TODO Auto-generated method stub
		count++;
		return 
		"Hello from first Spring Application";
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	

}
