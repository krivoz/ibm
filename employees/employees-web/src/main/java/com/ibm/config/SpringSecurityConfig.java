package com.ibm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;

@Configuration
@EnableWebSecurity(debug = true)
public class SpringSecurityConfig
        extends WebSecurityConfigurerAdapter {

    
    
     @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.ldapAuthentication()
            .userSearchBase("ou=people")
            .userSearchFilter("(uid={0})")
            .groupSearchBase("ou=groups")
            .groupSearchFilter("member={0}")
             
            .contextSource()
            .root("dc=baeldung,dc=com")
            .ldif("classpath:users.ldif");
    }
    
    
    
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth)
//            throws Exception {
//      auth.userDetailsService(
//                        (  username) ->  {
//                            //MoneyGramUser moneyGramUser = moneyGramUserService.findMoneyGramLoginByName(username);
//
//                            User.UserBuilder builder = User.withUsername("admin");
//                            if (true) {
//                                builder = org.springframework.security.core.userdetails.User.withUsername(username);
//                                builder.password("admin");
//                                builder.roles("USER");
//                                return builder.build();
//                            } else {
//                                throw new UsernameNotFoundException("User not found.");
//                                //return null;
//                            }
//
//                        });
//
//        
//        
//        
////        auth.inMemoryAuthentication()
////                .withUser("admin").password("{noop}admin").roles("ADMIN")
////                .and()
////                .withUser("user").password("{noop}user").roles("USER");
//        
//        
//        
////         auth.ldapAuthentication()
////            .userSearchBase("ou=people")
////            .userSearchFilter("(uid={0})")
////            .groupSearchBase("ou=groups")
////            .groupSearchFilter("member={0}")
////            .contextSource()
////            .root("dc=baeldung,dc=com")
////            .ldif("classpath:users.ldif");
//
////
////
//// ActiveDirectoryLdapAuthenticationProvider adProvider = 
////                    new ActiveDirectoryLdapAuthenticationProvider("ou=bluepages,o=ibm.com",
////                            "ldap://bluepages.ibm.com:389/");
////        adProvider.setConvertSubErrorCodesToExceptions(true);
////        adProvider.setUseAuthenticationRequestCredentials(true);
//// 
////        // set pattern if it exists
////        // The following example would authenticate a user if they were a member
////        // of the ServiceAccounts group
////        // (&(objectClass=user)(userPrincipalName={0})
////        //   (memberof=CN=ServiceAccounts,OU=alfresco,DC=mycompany,DC=com))
//////        if (userDNPattern != null && userDNPattern.trim().length() > 0)
//////        {
//////            adProvider.setSearchFilter(userDNPattern);
//////        }
////        auth.authenticationProvider(adProvider);
////        
////        // don't erase credentials if you plan to get them later
////        // (e.g using them for another web service call)
////        auth.eraseCredentials(false);
//
//    }
////    @Bean
////    public ActiveDirectoryLdapAuthenticationProvider activeDirectoryLdapAuthenticationProvider() {
////        ActiveDirectoryLdapAuthenticationProvider activeDirectoryLdapAuthenticationProvider = 
////                new ActiveDirectoryLdapAuthenticationProvider("CN=Users,DC=vac,DC=hu,DC=ibm,DC=com", "ldap://vaccua.vac.hu.ibm.com:389/");
////        return activeDirectoryLdapAuthenticationProvider;
////    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        
//          http
//                 .csrf().disable()
//                .authorizeRequests()
//                .anyRequest().authenticated()
//                .and()
//                .httpBasic();
          
          
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/pages/**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/**").permitAll()
                .antMatchers("/login*", "/applogin").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .usernameParameter("ibmusername")
                //.loginProcessingUrl("/applogin")
                //.loginPage("/login.jsf")
                .defaultSuccessUrl("/pages/main.jsf", true)
                //.failureUrl("/login.jsf?login_error=1")
                .and()
                .logout() //.logoutSuccessUrl("/login.jsf")
                ;
    }
}
